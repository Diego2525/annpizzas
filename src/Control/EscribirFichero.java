/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USRBET
 */
public class EscribirFichero {

    public EscribirFichero(String datos) {
        String ruta = "src/archivos/PizzaPrediccion.arff";
        File archivo = new File(ruta);
        BufferedWriter bw;
        try {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write("%Pizza\n"
                    + "@RALATION Pizza\n"
                    + "\n"
                    + "@ATTRIBUTE	Grosor				{Delgada,Media,Gruesa}\n"
                    + "@ATTRIBUTE	Queso				Real\n"
                    + "@ATTRIBUTE	Coccion				{Bajo,Medio,Alto}\n"
                    + "@ATTRIBUTE	Carne				{True,False}\n"
                    + "@ATTRIBUTE	NumIngredientes		Real\n" 
                    + "@ATTRIBUTE  Calidad				{Muy buena,Buena, Regular}n"
                    + "@DATA\n" + datos
            );
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(EscribirFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
