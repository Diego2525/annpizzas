package Agentes;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import annproyectoia.RedNeuronal;

public class Agente3 extends Agent {

    @Override
    protected void setup() {
        addBehaviour(new Comportamiento());
        super.setup();
    }

    @Override
    protected void takeDown() {
        super.takeDown();
    }

    class Comportamiento extends Behaviour {

        private boolean bandera = false;

        @Override
        public void action() {
            ACLMessage mensaj = blockingReceive();
            String m = (String) mensaj.getContent();
            System.out.println(m);
            new RedNeuronal().test();
            enviarMensaje("Ya se realizo el test", "Agente1", "id13");
            bandera=true;
        }

        @Override
        public boolean done() {
            return bandera;
        }

        private void enviarMensaje(String contenido, String receptor, String idConversacion) {
            //Comunicacion
            AID id = new AID();//agent ID
            id.setLocalName(receptor); //receptor
            ACLMessage mensaje = new ACLMessage(ACLMessage.REQUEST);
            mensaje.addReceiver(id);
            mensaje.setSender(getAID());
//            try {
//                mensaje.setContentObject((Serializable) contenido);
//            } catch (IOException ex) {
//                Logger.getLogger(Agente3.class.getName()).log(Level.SEVERE, null, ex);
//            }
            mensaje.setContent(contenido);
            mensaje.setLanguage("lol");
            mensaje.setConversationId(idConversacion);
            send(mensaje);
        }
        //Búsqueda
    }
}
