package Agentes;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import java.util.ArrayList;

public class Agente1 extends Agent {

    @Override
    protected void setup() {
//        System.out.println("Hola estoy aqui"+getName());
        addBehaviour(new Comportamiento());
        super.setup();
    }

    @Override
    protected void takeDown() {
        super.takeDown();
    }

    class Comportamiento extends Behaviour {

        ArrayList<ACLMessage> listaDeMensajeEntrada = new ArrayList();
        ArrayList<ACLMessage> listaDeMensajeSalida = new ArrayList();
        private boolean bandera = false;

        @Override
        public void action() {
            ACLMessage mensaje = blockingReceive();
            System.out.println(""+mensaje.getContent());
            enviarMensaje("Iniciar Test", "Agente3", "id13");
            mensaje = blockingReceive();
            System.out.println(""+mensaje.getContent());
            enviarMensaje("Iniciar Prediccion", "Agente4", "id14");
            mensaje = blockingReceive();
            System.out.println(""+mensaje.getContent());
            bandera=true;
        }

        @Override
        public boolean done() {
            return bandera;
        }

        private void enviarMensaje(String contenido, String receptor, String idConversacion) {
            //Comunicacion
            AID id = new AID();//agent ID
            id.setLocalName(receptor); //receptor
            ACLMessage mensaje = new ACLMessage(ACLMessage.REQUEST);
            mensaje.addReceiver(id);
            mensaje.setSender(getAID());
//            try {
//                mensaje.setContentObject((Serializable) contenido);
//            } catch (IOException ex) {
//                Logger.getLogger(Agente1.class.getName()).log(Level.SEVERE, null, ex);
//            }
            mensaje.setContent(contenido);
            mensaje.setLanguage("lol");
            mensaje.setConversationId(idConversacion);
            send(mensaje);
        }
        //Búsqueda
    }
}
