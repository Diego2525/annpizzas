package Agentes;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;
import  annproyectoia.RedNeuronal;
        
public class Agente2 extends Agent {

    @Override
    protected void setup() {
        addBehaviour(new Comportamiento());
        super.setup();
    }

    @Override
    protected void takeDown() {
        System.out.println("Adioss");
        super.takeDown();
    }

    class Comportamiento extends Behaviour {

        private boolean bandera = false;

        @Override
        public void action() {
            System.out.println("Iniciar entrenamiento");
            new RedNeuronal().entrenar("-L 0.2 -M 0.5 -N 1000 -V 0 -S 0 -E 20 -H 12");
            enviarMensaje("Ya se realizo el entrenamiento", "Agente1", "id12");
            bandera=true;
        }

        @Override
        public boolean done() {
            return bandera;
        }

        private void enviarMensaje(String contenido, String receptor, String idConversacion) {
            //Comunicacion
            AID id = new AID();//agent ID
            id.setLocalName(receptor); //receptor
            ACLMessage mensaje = new ACLMessage(ACLMessage.REQUEST);
            mensaje.addReceiver(id);
            mensaje.setSender(getAID());
//            try {
//                mensaje.setContentObject((Serializable) contenido);
//            } catch (IOException ex) {
//                Logger.getLogger(Agente2.class.getName()).log(Level.SEVERE, null, ex);
//            }
            mensaje.setContent(contenido);
            mensaje.setLanguage("lol");
            mensaje.setConversationId(idConversacion);
            send(mensaje);
        }
        //Búsqueda
    }
}
