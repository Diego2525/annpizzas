/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Contenedor;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import Agentes.*;
import jade.wrapper.StaleProxyException;
import java.util.logging.Level;
import java.util.logging.Logger;
//import tree.Tree;
/**
 *
 * @author Truth
 */
public class Container {
    
    AgentContainer mainContainer;
    
    public void contenedor(){
        jade.core.Runtime runtime = jade.core.Runtime.instance();
        runtime.setCloseVM(true);
        
        Profile profile = new ProfileImpl(null,6000,null); //(ip,puerto,nombre-unico)
        mainContainer = runtime.createMainContainer(profile);
        iniciarAgentes();
    }
    
    public void iniciarAgentes(){
        
        try {
            mainContainer.createNewAgent("Agente1", Agente1.class.getName(), null).start();
            mainContainer.createNewAgent("Agente3", Agente3.class.getName(), null).start();
            mainContainer.createNewAgent("Agente4", Agente4.class.getName(), null).start();
            mainContainer.createNewAgent("Agente2", Agente2.class.getName(), null).start();
        } catch (StaleProxyException ex) {
            Logger.getLogger(Container.class.getName()).log(Level.SEVERE, null, ex);
        }
  
    }
    
    public static void main(String[] args) {
        new Container().contenedor();
    }
    
}
