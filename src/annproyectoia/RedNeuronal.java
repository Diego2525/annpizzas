/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annproyectoia;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.Prediction;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.misc.SerializedClassifier;
import weka.core.Debug;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Utils;

/**
 *
 * @author USRBET
 */
public class RedNeuronal {

    public void entrenar(String paramANN) {
        try {
            FileReader trainReader = new FileReader("src/archivos/Pizza.arff");
            //instancias
            Instances train = new Instances(trainReader);
            train.setClassIndex(train.numAttributes() - 1);

            //multicapa perceptron
            MultilayerPerceptron mlp = new MultilayerPerceptron();
            mlp.setOptions(Utils.splitOptions(paramANN));
            mlp.buildClassifier(train);
            Debug.saveToFile("ModeloEntrenado.train", mlp);

            //serializar
            SerializedClassifier classifier = new SerializedClassifier();
            classifier.setModelFile(new File("ModeloEntrenado.train"));

            //evaluar modelo
            Evaluation evaluation = new Evaluation(train);
            evaluation.evaluateModel(classifier, train);
            System.out.println(evaluation.toSummaryString());
            System.out.println(evaluation.toMatrixString("Resultados Matrix"));

        } catch (FileNotFoundException ex) {
            Logger.getLogger(RedNeuronal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RedNeuronal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(RedNeuronal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void test() {

        try {
            FileReader testReader = new FileReader("src/archivos/PizzaTest.arff");
            Instances test = new Instances(testReader);

            test.setClassIndex(test.numAttributes() - 1);
            SerializedClassifier classifier = new SerializedClassifier();
            classifier.setModelFile(new File("ModeloEntrenado.train"));
            Classifier mlp = classifier.getCurrentModel();
            Evaluation evalTest = new Evaluation(test);
            evalTest.evaluateModel(mlp, test);
            
            System.out.println(evalTest.toSummaryString());
            System.out.println(evalTest.toMatrixString("Resultados Matrix"));
            testReader.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(RedNeuronal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RedNeuronal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(RedNeuronal.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public double prediccion() {
        FileReader testReader;
        double n = -1;
        try {
            testReader = new FileReader("src/archivos/PizzaPrediccion.arff");
            Instances test = new Instances(testReader);
            test.setClassIndex(test.numAttributes() - 1);
            SerializedClassifier classifier = new SerializedClassifier();
            classifier.setModelFile(new File("ModeloEntrenado.train"));
            Classifier mlp = classifier.getCurrentModel();
            Evaluation evalTest = new Evaluation(test);
            evalTest.evaluateModel(mlp, test);
            ArrayList<Prediction> prediccion = evalTest.predictions();
            for (Iterator<Prediction> iterator = prediccion.iterator(); iterator.hasNext();) {
                Prediction p = iterator.next();
//                System.out.println(p.predicted()+" prediccion");
//                System.out.println(p.actual()+" actual\n");
                n = p.predicted();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RedNeuronal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RedNeuronal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(RedNeuronal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public static void main(String[] args) {
//         new RedNeuronal().entrenar("-L 0.2 -M 0.5 -N 2000 -V 0 -S 0 -E 20 -H 12");
//         new RedNeuronal().entrenar("");
//         new RedNeuronal().test();
        new RedNeuronal().prediccion();
    }
}
